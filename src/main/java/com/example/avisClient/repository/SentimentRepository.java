package com.example.avisClient.repository;

import com.example.avisClient.entity.Sentiment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SentimentRepository extends JpaRepository<Sentiment,Integer>
{

}
