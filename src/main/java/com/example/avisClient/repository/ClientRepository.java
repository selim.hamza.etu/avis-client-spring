package com.example.avisClient.repository;

import com.example.avisClient.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer> {

    public Client findClientsByEmail(String email);
}
