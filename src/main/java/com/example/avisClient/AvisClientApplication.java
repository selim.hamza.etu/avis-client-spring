package com.example.avisClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvisClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvisClientApplication.class, args);
	}

}
