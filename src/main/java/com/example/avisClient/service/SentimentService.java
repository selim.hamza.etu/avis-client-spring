package com.example.avisClient.service;

import com.example.avisClient.entity.Client;
import com.example.avisClient.entity.Sentiment;
import com.example.avisClient.repository.SentimentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SentimentService {

    private ClientService clientService;
    @Autowired
    private SentimentRepository sentimentRepository;

    public SentimentService(SentimentRepository sentimentRepository, ClientService clientService) {
        this.clientService = clientService;
        this.sentimentRepository = sentimentRepository;
    }

    public void create(Sentiment sentiment)
    {
        Client client = clientService.ReadOrCreat(sentiment.getClient());
        sentiment.setClient(client);
        this.sentimentRepository.save(sentiment);
    }

    public List<Sentiment> lireAll()
    {
        return this.sentimentRepository.findAll();
    }

    public Sentiment lire(int id)
    {
        Optional<Sentiment> res = sentimentRepository.findById(id);
        if (res.isPresent())
        {
            return res.get();
        }
        return null;
    }

    public void deleteById(int id)
    {
        sentimentRepository.deleteById(id);
    }
}
