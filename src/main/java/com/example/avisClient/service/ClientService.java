package com.example.avisClient.service;

import com.example.avisClient.entity.Client;
import com.example.avisClient.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {


    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public ClientRepository getClientRepository() {
        return clientRepository;
    }

    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    private ClientRepository clientRepository;
    public List<Client> lireAll()
    {
        return this.clientRepository.findAll();
    }

    public Client lire(int id)
    {
        Optional<Client> optionalClient = this.clientRepository.findById(id);
        if (optionalClient.isPresent())
        {
            return optionalClient.get();
        }
        else return null;
    }

    public void create(Client client)
    {
        Client clientInBDD = this.clientRepository.findClientsByEmail(client.getEmail());
        if (clientInBDD == null)
        {
            this.clientRepository.save(client);
        }


    }

    public void deleteById(int id)
    {
        this.clientRepository.deleteById(id);
    }

    public Client ReadOrCreat(Client clientAcreer) {
        Client clientExist = clientRepository.findClientsByEmail(clientAcreer.getEmail());
        if (clientExist == null)
        {
            clientExist = clientRepository.save(clientAcreer);
        }
        return clientExist;
    }
}
