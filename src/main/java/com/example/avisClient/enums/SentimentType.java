package com.example.avisClient.enums;

public enum SentimentType {
    POSITIF,
    NEGATIF
}
