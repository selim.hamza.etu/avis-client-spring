package com.example.avisClient.entity;

import com.example.avisClient.enums.SentimentType;
import jakarta.persistence.*;

import static jakarta.persistence.CascadeType.MERGE;
import static jakarta.persistence.CascadeType.PERSIST;

@Entity
@Table(name = "Sentiment")
public class Sentiment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String texte;
    private SentimentType type;
    @ManyToOne(cascade = {PERSIST, MERGE}) // Permet de créer client et sentiment en meme temps si existe pas +
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    public Sentiment(String texte, SentimentType type, Client client) {
        this.texte = texte;
        this.type = type;
        this.client = client;
    }

    public Sentiment() {

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public SentimentType getType() {
        return type;
    }

    public void setType(SentimentType type) {
        this.type = type;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
