package com.example.avisClient.controller;

import com.example.avisClient.entity.Client;
import com.example.avisClient.service.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static jakarta.annotation.Resource.AuthenticationType.APPLICATION;

@RestController
@RequestMapping(path = "client", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientController  {

    private ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping
    public void create(@RequestBody Client client)
    {
        this.clientService.create(client);
    }


    @DeleteMapping
    public void deleteById(int id)
    {
        this.clientService.deleteById(id);
    }

    @GetMapping
    public List<Client> lireAll()
    {
        return this.clientService.lireAll();
    }

    @GetMapping(path =  "{id}")
    public Client lire(@PathVariable int id)
    {
        return this.clientService.lire(id);
    }
}
