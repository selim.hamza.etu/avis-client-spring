package com.example.avisClient.controller;

import com.example.avisClient.entity.Sentiment;
import com.example.avisClient.service.SentimentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "sentiment")
public class SentimentController
{
    private SentimentService sentimentService;

    public SentimentController(SentimentService sentimentService)
    {
        this.sentimentService = sentimentService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping
    public void create(@RequestBody Sentiment sentiment)
    {
        this.sentimentService.create(sentiment);
    }

    @DeleteMapping
    public void deleteById(int id)
    {
        this.sentimentService.deleteById(id);
    }

    /*@GetMapping
    public Sentiment lire(int id)
    {
        return this.sentimentService.lire(id);
    }*/


    @GetMapping
    public @ResponseBody List<Sentiment> lireAll()
    {
        return this.sentimentService.lireAll();
    }
}
